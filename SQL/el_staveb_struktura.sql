CREATE DATABASE IF NOT EXISTS polachova_i4c DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci;
USE polachova_i4c;

DROP TABLE IF EXISTS firmy;
CREATE TABLE IF NOT EXISTS firmy (
  id varchar(20) COLLATE utf8_czech_ci NOT NULL,
  heslo varchar(40) COLLATE utf8_czech_ci NOT NULL COMMENT 'sha1 hash',
  kod int(6) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

DROP TABLE IF EXISTS typ;
CREATE TABLE IF NOT EXISTS typ (
  id int(7) NOT NULL AUTO_INCREMENT,
  id_firmy varchar(20) COLLATE utf8_czech_ci NOT NULL,
  typ_souc varchar(30) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT typ_idf_fk FOREIGN KEY (id_firmy) REFERENCES firmy(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS souc;
CREATE TABLE IF NOT EXISTS souc (
  id int(7) NOT NULL AUTO_INCREMENT,
  kat_c_souc varchar(11) COLLATE utf8_czech_ci NOT NULL,
  id_firmy varchar(20) COLLATE utf8_czech_ci NOT NULL,
  typ_souc int(7) NOT NULL,
  hodnota_souc varchar(31) COLLATE utf8_czech_ci NOT NULL,
  provedeni_souc varchar(3) COLLATE utf8_czech_ci DEFAULT NULL,
  pouzdro_souc varchar(31) COLLATE utf8_czech_ci DEFAULT NULL,
  cena_souc float DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT souc_idf_fk FOREIGN KEY (id_firmy) REFERENCES firmy(id),
  CONSTRAINT souc_idt_fk FOREIGN KEY (typ_souc) REFERENCES typ(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS staveb;
CREATE TABLE IF NOT EXISTS staveb (
  id int(7) NOT NULL AUTO_INCREMENT,
  kat_c_staveb varchar(15) COLLATE utf8_czech_ci NOT NULL,
  id_firmy varchar(20) COLLATE utf8_czech_ci NOT NULL,
  nazev_staveb varchar(31) COLLATE utf8_czech_ci NOT NULL,
  cely_nazev_staveb varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT staveb_idf_fk FOREIGN KEY (id_firmy) REFERENCES firmy(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS seznam;
CREATE TABLE IF NOT EXISTS seznam (
  id int(7) NOT NULL AUTO_INCREMENT,
  id_souc_seznam int(7) NOT NULL,
  id_staveb_seznam int(7) NOT NULL,
  id_firmy varchar(20) COLLATE utf8_czech_ci NOT NULL,
  pocet_ks_souc_staveb int(7) DEFAULT '0',
  PRIMARY KEY (id),
  CONSTRAINT seznam_idf_fk FOREIGN KEY (id_firmy) REFERENCES firmy(id),
  CONSTRAINT seznam_idso_fk FOREIGN KEY (id_souc_seznam) REFERENCES souc(id),
  CONSTRAINT seznam_idst_fk FOREIGN KEY (id_staveb_seznam) REFERENCES staveb(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;