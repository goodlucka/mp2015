<?php
 require_once './db.php';
 session_start();
 header('Content-type: text/html; charset=utf-8');
 
 if(!isset($_SESSION["id"])) echo "<p class='chyba'>Stránka je pouze pro přihlášené firmy</p>";
  else {
?>
<h3>
 Stavebnice
</h3>   
 <div id="vyhledavani_hl" class="form">
  <div class="form_oddil">
    <div class="vyhledavani">
      <span>Kat.č.:</span>
      <input type="text" id="form_kat_c">
    </div>
    <div class="vyhledavani">
      <span>Název:</span>
      <input type="text" id="form_nazev">
    </div>
    <div class="vyhledavani">
      <span>Celý název:</span>
      <input type="text" id="form_cely_nazev">
    </div>
  </div>
  <div class="form_oddil">
   <div class="vyhledavani">
      <span>Řadit dle:</span>
      <select id="form_radit_dle">
        <option value="kat_c_staveb">Katalogové číslo</option>
        <option value="nazev_staveb">Název</option>
        <option value="cely_nazev_staveb">Celý název</option>
      </select>
    </div>
   <div class="vyhledavani">
      <span>Řazení:</span>
      <select id="form_radit">
        <option value="ASC">A->Z (0->9)</option>
        <option value="DESC">Z->A (9->0)</option>
      </select>
    </div>
  </div>
  <div class="form_oddil">
    <div class="vyhledavani_button">
      <span class="mbtn" onclick="stavebVyhledat()">Hledat</span>
    </div>
  </div>
 </div>
  
 <div class="clear_both">
 </div>
 
 <div id="vyhledavani">
 </div>
  
 <div id="konec">
 </div>
<?php
  }  
?>