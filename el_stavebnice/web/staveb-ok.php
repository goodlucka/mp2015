<?php
 require_once './db.php';
 session_start();
 header('Content-type: text/html; charset=utf-8');
 if(!isset($_SESSION["id"])) echo "<p class='chyba'>Stránka je pouze pro přihlášené firmy</p>";
  else {
   $kat_c=addslashes(rawurldecode($_GET["kat_c"]));
   $nazev=addslashes(rawurldecode($_GET["nazev"]));
   $cely_nazev=addslashes(rawurldecode($_GET["cely_nazev"]));
   $ok=0;
   if(strlen($kat_c)>15 || strlen($kat_c)<1) echo "<p class='chyba'>Katalogové číslo stavebnice není v daném rozsahu, min. počet znaků je 1, max. počet znaků je 15</p>";
    else $ok++;
   if(strlen($nazev)>31 || strlen($nazev)<1) echo "<p class='chyba'>Název stavebnice není v daném rozsahu, min. počet znaků je 1, max. počet znaků je 31</p>";
    else $ok++;
   if(strlen($cely_nazev)>100 || strlen($cely_nazev)<1) echo "<p class='chyba'>Celý název stavebnice není v daném rozsahu, min. počet znaků je 1, max. počet znaků je 100</p>";
    else $ok++;
   if($ok==3) echo "<span class='mbtn' onclick='stavebPridat()'>Přidat stavebnici</span>"; 
 }
?>