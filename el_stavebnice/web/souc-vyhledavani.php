<?php
 require_once './db.php';
 session_start();
 header('Content-type: text/html; charset=utf-8');

 if(!isset($_SESSION["id"])) echo "<p class='chyba'>Stránka je pouze pro přihlášené firmy</p>";
  else {
?>

<h3>
 Součástky
</h3>

 <div class="form" id="vyhledavani_hl">
  <div class="form_oddil">
    <div class="vyhledavani">
      <span>Kat.č.:</span>
      <input type="text" id="form_kat_c">
    </div>
    <div class="vyhledavani select">
      <span>Typ:</span>
      <select id="form_typ">
      <?php
       $sql="SELECT typ_souc,id FROM typ WHERE id_firmy LIKE '".$_SESSION["id"]."' ORDER BY typ_souc ASC";
       $vysledek=mysql_query($sql,$link);
       $echo="<option value=''>Vše</option>";
       while($row=mysql_fetch_array($vysledek))
       {
        $echo.="<option value='".$row["id"]."'>".$row["typ_souc"]."</option>";
       }
       echo $echo;
      ?>
      </select>
    </div>    
    <div class="vyhledavani select">
      <span>Provedení:</span>
      <select id="form_provedeni">      
         <option value="%">Vše</option>
         <option value="">Neurčeno</option>
         <option value="SMD">SMD</option>
         <option value="THT">THT</option>
      </select>
    </div>
  </div>
  <div class="form_oddil">
    <div class="vyhledavani">
      <span>Hodnota:</span>
      <input type="text" id="form_hodnota">
    </div>
    <div class="vyhledavani">
      <span>Pouzdro:</span>
      <input type="text" id="form_pouzdro">
    </div>
  </div>
  <div class="form_oddil">
    <div class="vyhledavani">
      <span>Cena od:</span>
      <input type="text" id="form_cena_od">
    </div>
    <div class="vyhledavani">
      <span>Cena do:</span>
      <input type="text" id="form_cena_do">
    </div>
  </div>
  <div class="form_oddil">
    <div class="vyhledavani">
      <span>Řadit dle:</span>
      <select id="form_radit_dle">
        <option value="kat_c_souc">Katalogové číslo</option>
        <option value="typ_souc">Typ</option>
        <option value="hodnota_souc">Hodnota</option>
        <option value="provedeni_souc">Provedení</option>
        <option value="pouzdro_souc">Pouzdro</option>
        <option value="cena_souc">Cena</option>
      </select>
    </div>
    <div class="vyhledavani">
      <span>Řazení:</span>
      <select id="form_radit">
        <option value="ASC">A->Z (0->9)</option>
        <option value="DESC">Z->A (9->0)</option>
      </select>
    </div>
  </div>
  <div class="form_oddil">
    <div class="vyhledavani_button">
      <span class="mbtn" onclick="soucVyhledat()">Hledat</span>
    </div>
  </div>
 </div>
  
 <div class="clear_both">
 </div>
 
 <div id="vyhledavani">
 </div>
 

 <div id="konec">
 </div>
<?php
  } 
?>