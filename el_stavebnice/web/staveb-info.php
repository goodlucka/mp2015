<?php        
 require_once './db.php';
 session_start();
 header('Content-type: text/html; charset=utf-8');
 if(!isset($_SESSION["id"])) echo "<p class='chyba'>Stránka je pouze pro přihlášené firmy</p>";
  else {                                                     
   echo "<div id='info'>";       
   echo "<span class='x' onclick=\"stavebX()\"></span>";   
   $echo="";     
   $id=$_GET["id"];
   if(isset($_GET["idso"])) $idso=$_GET["idso"];
     else $idso=0;
   if(isset($_GET["pocet"]))
   {
     $pocet=(int)rawurldecode($_GET["pocet"]);
     if($pocet<1) echo "<p class='chyba'>Počet kusů musí být větší než 0</p>";
     else {
       if(isset($_GET["idseu"]))
       {
         $sql="UPDATE seznam SET pocet_ks_souc_staveb=".$pocet." WHERE id=".$_GET["idseu"];
         $vysledek=mysql_query($sql,$link);
       }
       if(isset($_GET["idsop"]))
       {
         $sql="INSERT INTO seznam (id_souc_seznam,id_staveb_seznam,pocet_ks_souc_staveb,id_firmy) VALUES (".$_GET["idsop"].",".$id.",".$pocet.",'".$_SESSION["id"]."')";
         $vysledek=mysql_query($sql,$link);   
       }
     }
   }
 
   if(isset($_GET["id"])&isset($_GET["kat_c"]))
   { 
     $kat_c=addslashes(rawurldecode($_GET["kat_c"]));
     $nazev=addslashes(rawurldecode($_GET["nazev"]));
     $cely_nazev=addslashes(rawurldecode($_GET["cely_nazev"])); 
     $ok=0;
     if(strlen($kat_c)>15 || strlen($kat_c)<1) echo "<p class='chyba'>Katalogové číslo stavebnice není v daném rozsahu, min. počet znaků je 1, max. počet znaků je 15</p>";
       else $ok++;
     if(strlen($nazev)>31 || strlen($nazev)<1) echo "<p class='chyba'>Název stavebnice není v daném rozsahu, min. počet znaků je 1, max. počet znaků je 31</p>";
       else $ok++;
     if(strlen($cely_nazev)>100 || strlen($cely_nazev)<1) echo "<p class='chyba'>Celý název stavebnice není v daném rozsahu, min. počet znaků je 1, max. počet znaků je 100</p>";
       else $ok++;
     $sql="SELECT id FROM staveb WHERE kat_c_staveb LIKE '".$kat_c."' AND id_firmy LIKE '".$_SESSION["id"]."' AND id!=".$id;
     $vysledek=mysql_query($sql,$link);
     if(($row=mysql_fetch_array($vysledek))!=null) echo "<p class='chyba'>Zadaná stavebnice s katalogovým číslem již existuje</p>";
       else $ok++; 
     if($ok==4)
     {
       $sql="UPDATE staveb SET kat_c_staveb='".$kat_c."', nazev_staveb='".$nazev."', cely_nazev_staveb='".$cely_nazev."' WHERE id=".$id;  
       $vysledek=mysql_query($sql,$link) or die("<p class='chyba'>Chyba při ukládání stavebnice</p>");
       echo "<p class='ok'>Změny byly uloženy</p>";
     }
   }   
 
   if(isset($_GET["idse"]))
   {
     $sql="DELETE FROM seznam WHERE id=".$_GET["idse"];
     $vysledek=mysql_query($sql,$link) or die("<p>Součástka nebyla odebrána ze stavebnice</p>");
   }        
   
   $sql="SELECT st.id,st.kat_c_staveb, st.nazev_staveb, st.cely_nazev_staveb FROM staveb st ";
   $sql.="WHERE st.id_firmy LIKE '".$_SESSION["id"]."' AND st.id=".$id;
   $vysledek=mysql_query($sql,$link) or die("<p class='chyba'>Chyba při hledání stavebnice</p>");
   $row=mysql_fetch_array($vysledek);       
   $echo="<div class='form'><div><span>Katalogové číslo: </span>";
   $echo.="<div class='form_d'><input type='text' id='form_i_kat_c' value='".$row["kat_c_staveb"]."'></div></div>";
   $echo.="<div><span>Název: </span><div class='form_d'><input type='text' id='form_i_nazev' value='".$row["nazev_staveb"]."'></div></div>";
   $echo.="<div><span>Celý název: </span><div class='form_d'><input type='text' id='form_i_cely_nazev' value='".$row["cely_nazev_staveb"]."'></div></div>";
   $echo.="<div><span class='mbtn' onclick=\"stavebUprava('".$id."')\">Upravit stavebnici</span></div>";
   $echo.="</div>";
   $echo.="<span class='mbtn' onclick=\"stavebOdstranit('".$id."')\">Odstranit</span>";
   echo $echo;
  
   $echo="<p>V této stavebnici jsou použity tyto součástky:</p>";
   $sql="SELECT so.id AS idso, so.kat_c_souc, t.typ_souc, so.hodnota_souc, so.provedeni_souc, so.pouzdro_souc, so.cena_souc, se.id AS idse, se.pocet_ks_souc_staveb AS pocet ";
   $sql.="FROM typ t INNER JOIN souc so ON t.id=so.typ_souc INNER JOIN seznam se ON so.id=se.id_souc_seznam WHERE se.id_staveb_seznam=".$id." ORDER BY t.typ_souc ASC, so.kat_c_souc ASC";
   $vysledek=mysql_query($sql,$link);
   $i=0;    
   $ttr=0;
   $echo.="<table class='table_8'><thead><tr><th>Kat.č.</th><th>Typ</th><th>Hodnota</th><th>Provedení</th><th>Pouzdro</th><th>Cena</th><th>Počet ks</th><th>Odstranit</th></tr></thead><tbody>";
   while($row=mysql_fetch_array($vysledek))
   {
    $echo.="<tr class='table_tr_".$ttr."'><td>".$row["kat_c_souc"]."</td><td>".$row["typ_souc"]."</td><td>".$row["hodnota_souc"]."</td>";
    $echo.="<td>".$row["provedeni_souc"]."</td><td>".$row["pouzdro_souc"]."</td><td>".$row["cena_souc"]."</td>";
    if($row["idso"]==$idso) $echo.="<td><input type='text' id='i".$row["idse"]."' value='".$row["pocet"]."' onchange=\"stavebPocetUpravitOk('".$id."','".$row["idse"]."')\" size=3></td>";
    else $echo.="<td onclick=\"stavebPocetUpravit('".$id."','".$row["idso"]."')\">".$row["pocet"]."</td>";
    $echo.="<td><span class='mbtn' onclick=\"stavebSoucOdstranit('".$id."','".$row["idse"]."')\">Odstranit</span></td>";
    $i++;   
    if($ttr==0) $ttr++;
     else $ttr--;
   }
   $echo.="</tbody></table>";
   if($i!=0) echo $echo;
   else echo "<p>Tato stavebnice nemá žádné součástky</p>";
 
   $echo="<div><h3>Přidání součástek do stavebnice</h3></div>";
   $sql="SELECT DISTINCT so.id,so.kat_c_souc,t.typ_souc,so.hodnota_souc,so.provedeni_souc,so.pouzdro_souc,so.cena_souc ";
   $sql.="FROM typ t INNER JOIN souc so ON t.id=so.typ_souc LEFT JOIN seznam se ON so.id=se.id_souc_seznam ";
   $sql.="WHERE so.id NOT IN(SELECT id_souc_seznam FROM seznam WHERE id_staveb_seznam=".$id.") AND so.id_firmy LIKE '".$_SESSION["id"]."' ";
   $sql.="ORDER BY t.typ_souc ASC, so.kat_c_souc ASC";
   $vysledek=mysql_query($sql,$link);
   $echo.="<table class='table_8'><thead><tr><th>Kat.č.</th><th>Typ</th><th>Hodnota</th><th>Provedení</th><th>Pouzdro</th><th>Cena</th><th>Počet ks</th><th>Přidat</th></tr></thead><tbody>";
   $i=0;
   $ttr=0;
   $row=mysql_fetch_array($vysledek);
   while($row)
   {                  
    $pom=$row;
    $echo.="<tr><th colspan='8' class='table_td_th'>Typ: ".$row["typ_souc"]."</th></tr>";
    while($pom["typ_souc"]==$row["typ_souc"])
    {  
      $echo.="<tr class='table_tr_".$ttr."'><td>".$row["kat_c_souc"]."</td><td>".$row["typ_souc"]."</td><td>".$row["hodnota_souc"]."</td>";
      $echo.="<td>".$row["provedeni_souc"]."</td><td>".$row["pouzdro_souc"]."</td><td>".$row["cena_souc"]."</td>";
      $echo.="<td><input type='text' id='poc".$row["id"]."' value='0' size=3></td><td><span class='mbtn' onclick=\"stavebSoucPridat('".$id."','".$row["id"]."')\">Přidat</span></td>";        
      if($ttr==0) $ttr++;
       else $ttr--;
      $row=mysql_fetch_array($vysledek);   
    }
    $i++;    
   }
   $echo.="</tbody></table>";
   if($i!=0) echo $echo;
   else echo "<p>Nelze přidat další součástku do stavebnice</p>";            
   echo "</div>";       
 }
?>
