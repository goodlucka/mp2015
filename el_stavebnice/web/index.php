<div id="index">
 <h2>
 Manuál
</h2>
<div>
  <h3>
   Registrace
  </h3>
  <p>
   Prvním krokem je registrace. Klikneme vpravo nahoře na nápis Registrace a poté vyplníme do daných polí své přihlašovací jméno a kód, který slouží ke generování nového hesla, pokud zapomeneme staré. Po správném vyplnění daných políček se zobrazí tlačítko Registrovat. Po registraci se objeví přihlašovací heslo, které si buď zkopírujeme, nebo někam zapíšeme a můžeme se přihlásit. Přihlásíme se kliknutím na nápis Přihlášení, které je vpravo nahoře, následně vyplníme přihlašovací jméno a heslo a klikneme na tlačítko Přihlásit. V případě zapomenutého hesla klikneme na tlačítko Zapomenuté heslo, kde napíšeme přihlašovací jméno a kód, který jsme zadali při registraci. Poté se nám zobrazí nové heslo a můžeme se přihlásit.
  </p>
</div>

<div>
  <h3>
   Přihlášení
  </h3>
  <p>
   Po přihlášení můžeme upravit kód či heslo, a to kliknutím vpravo nahoře na Profil firmy xxx a poté kliknutím na tlačítko Změnit kód nebo Změnit heslo. Po zadání správných hodnot do textového pole se objeví tlačítko Upravit kód nebo Upravit heslo. Po kliknutí na toto tlačítko se upraví kód nebo heslo. 
  </p>
</div>

<div>
  <h3>
   Odstranění firmy
  </h3>
  <p>
   Ke správě firmy patří i smazání firmy. Firmu odstraníme kliknutím na nápis Smazat firmu, který je opět vpravo nahoře celé stránky a poté potvrdíme kliknutím na tlačítko Smazat firmu.
  </p>
</div>

<div>
  <h3>
   Přidávání
  </h3>
  <p>
   Pro používání databáze potřebujeme zadat všechna data. Začneme nejdříve typem součástky, poté součástkou a nakonec stavebnicí. Poté můžeme propojit součástky se stavebnicemi.
  </p>
  <p>
   Typ součástky přidáme tak, že najedeme kurzorem v menu na Součástky a poté na Přidání typu součástky. Do textového pole zadáme název typu součástky, který jsme ještě nezadali (např. kondenzátor, dioda). Typ součástky může mít nejvýše 30 znaků. V případě správného zadání se zobrazí tlačítko pro přidání typu součástky.
  </p>
  <p>
   Druhým krokem je přidání součástky. Součástku přidáme tak, že najedeme kurzorem myši v menu na Součástky a poté na Přidání součástky. Postupně vyplníme alespoň povinná pole, která jsou označena hvězdičkou. V případě zadání ceny součástky oddělujeme na koruny a halíře. Po vyplnění povinných hodnot se zobrazí tlačítko pro přidání součástky.
  </p>
  <p>
   Další krok je přidání stavebnice. Stejně jako u přidávání součástky, tak i zde musíme najet kurzorem myši na Stavebnice a poté na Přidání stavebnice a vyplnit všechna pole. Po vyplnění katalogového čísla, názvu a celého názvu se zobrazí tlačítko pro přidání stavebnice.
  </p>
</div>

<div>
  <h3>
   Vyhledávání
  </h3>
  <p>
   Po přidání typu součástek, součástek a stavebnic, je můžeme vyhledat a následně přiřadit součástky ke stavebnicím. Najedeme kurzorem myši na Součástky nebo Stavebnice a pak na dané vyhledávání. Ve vyhledávání typu součástky máme pouze textové pole pro zadání typu součástky a vzestupné nebo sestupné řazení. Ve vyhledávání součástek a stavebnic je více parametrů, podle čeho chceme danou součástku vyhledat včetně toho, podle čeho chceme danou součástku či stavebnici řadit a jak. Po zadání parametrů klikneme na tlačítko Hledat a vypíše se nám tabulka s vyhledanými záznamy. Po kliknutí na řádek v tabulce se otevře informace o typu součástky, součástce nebo stavebnici (dle toho, co hledáme). Stránka o informacích je rozdělena do tří bloků dle následujícího schématu:
   <img src="../styl/blok.png">
  </p>
  <p>
   Blok A slouží k úpravě a odstranění, popř. zavření informace a přejití zpět na vyhledávání. Blok B obsahuje tabulku záznamů, kam patří náš vyhledaný záznam. V bloku C najdeme všechny ostatní záznamy, které k našemu záznamu nepatří.
  </p>
</div>

<div>
  <h3>
   Informace o typu součástky
  </h3>
  <p>
   V informacích o daném typu součástky jsou v bloku B součástky s použitým typem. Po kliknutí na součástku se zobrazí výběrové pole pro změnu typu součástky, protože nemůžeme smazat typ součástky, který už v nějaké součástce existuje. Při vybrání jiného typu součástky se vše aktualizuje a součástka bude mít jiný typ. V bloku C jsou ostatní součástky, kde není tento typ použit. Pro efektivní přidávání je vpravo tlačítko pro přiřazení součástky k danému typu.
  </p>
</div>

<div>
  <h3>
   Informace o součástce
  </h3>
  <p>
   Na stránce informace o součástce jsou v bloku B stavebnice, ve kterých je už přiřazená daná součástka. Kliknutím na sloupec Počet ks můžeme změnit počet kusů. Zadáme jinou hodnotu a potvrdíme klávesou Enter. Tato hodnota musí být větší než 0. Pokud chceme součástku ze stavebnice odstranit, klikneme na tlačítko Odstranit, které je vpravo u dané stavebnice. V bloku C jsou vypsány všechny stavebnice, ve kterých daná součástka není. Pro přidání součástky do stavebnice napíšeme ke každému řádku počet kusů a v tom daném řádku klikneme na Přidat.
  </p>
</div>

<div>
  <h3>
   Informace o stavebnici
  </h3>
  <p>
   V bloku B v informacích o stavebnicích jsou vypsány všechny součástky, které patří k dané stavebnici. Po kliknutí na sloupec Počet ks můžeme změnit počet kusů a opět potvrdíme klávesou Enter. Pokud chceme danou součástku ze stavebnice odstranit, klikneme na tlačítko Odstranit v příslušném řádku. V bloku C jsou vypsány všechny součástky, které nepatří k dané stavebnici, řazené dle jejich typu a poté dle jejich katalogového čísla. Pokud chceme součástku přidat do dané stavebnice, vyplníme počet kusů a poté v daném řádku klikneme na Přidat.
  </p>
</div>

<div>
  <h3>
   Balení součástek
  </h3>
  <p>
   Po celkové správě součástek a stavebnic se můžeme přesunout k jejich balení, a to kliknutím v menu na Balení součástek. Klikneme na řádek stavebnice, kterou chceme připsat, poté se ve sloupci Počet kusů objeví textové pole pro zadání počtu kusů stavebnic. Po zadání správného počtu kusů potvrdíme klávesou Enter. Následně klikneme na tlačítko Výpis, které je nad tabulkou.
  </p>
  <p>
   Na stránce s výpisem se zobrazí seznam všech součástek, které potřebujeme k těmto stavebnicím. Chceme-li tuto tabulku vytisknout, klikneme na tlačítko Vytisknout. Pokud jsme se spletli, můžeme změnit počet kusů a to kliknutím na příslušné tlačítko nad tabulkou. Pokud přejdeme na jinou stránku, počet kusů stavebnic se automaticky vynuluje.
  </p>
</div>
</div>