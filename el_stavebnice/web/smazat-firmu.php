<?php
 require_once './db.php';
 session_start();
 header('Content-type: text/html; charset=utf-8');
 if(!isset($_SESSION["id"])) echo "<p class='chyba'>Stránka je pouze pro přihlášené firmy</p>";
  else {
?>

<p class="ok">
 Opravdu si přejete smazat Vaši firmu? Tato akce se nedá vrátit, smaže se vše včetně dat.
 <br>
</p>                                                                  
 <span class='mbtn' onclick="smazatFirmu()">Smazat firmu</span>
<?php
 }
?>