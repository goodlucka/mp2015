<?php
 require_once './db.php';
 session_start();
 header('Content-type: text/html; charset=utf-8');
 
 $kat_c=addslashes(rawurldecode($_GET["kat_c"]));
 $hodnota=addslashes(rawurldecode($_GET["hodnota"]));
 $pouzdro=addslashes(rawurldecode($_GET["pouzdro"]));
 $ok=0;
 if(strlen($kat_c)>15 || strlen($kat_c)<1) echo "<p class='chyba'>Katalogové číslo součástky není v daném rozsahu, min. počet znaků je 1, max. počet znaků je 15</p>";
  else $ok++;
 if(strlen($hodnota)>31 || strlen($hodnota)<1) echo "<p class='chyba'>Hodnota součástky není v daném rozsahu, min. počet znaků je 1, max. počet znaků je 31</p>";
  else $ok++;
 if($pouzdro!=NULL)
 {
   if(strlen($pouzdro)>31) echo "<p class='chyba'>Pouzdro součástky je moc dlouhé, max. počet znaků je 31</p>";
     else $ok++;                                                                                              
 } else $ok++;
 if($ok==3) echo "<span class='mbtn' onclick='soucPridat()'>Přidat součástku</span>"; 
?>