<?php
 require_once './db.php';
 session_start();
 header('Content-type: text/html; charset=utf-8');
 if(!isset($_SESSION["id"])) echo "<p class='chyba'>Stránka je pouze pro přihlášené firmy</p>";
  else {
?>              
<h3>
 Přidání součástky
</h3>

<?php
    if(isset($_GET["kat_c"]))
    {
      $kat_c=addslashes(rawurldecode($_GET["kat_c"]));
      $typ=addslashes(rawurldecode($_GET["typ"]));
      $hodnota=addslashes(rawurldecode($_GET["hodnota"])); 
      $provedeni=addslashes(rawurldecode($_GET["provedeni"]));
      $pouzdro=addslashes(rawurldecode($_GET["pouzdro"]));
      $cena_kc=(int)rawurldecode($_GET["cena_kc"]);
      $cena_h=(int)rawurldecode($_GET["cena_h"]);
      $cena=$cena_kc.".".$cena_h;
      $cena=(float)$cena;
      $sql="INSERT INTO souc (kat_c_souc,id_firmy,typ_souc,hodnota_souc,provedeni_souc,pouzdro_souc,cena_souc) ";
      $sql.="VALUES ('".$kat_c."','".$_SESSION["id"]."','".$typ."','".$hodnota."','".$provedeni."','".$pouzdro."','".$cena."')";
      $vysledek=mysql_query($sql,$link);
      echo "<p class='ok'>Součástka přidána.</p>";
    }   
    $sql="SELECT * FROM typ WHERE id_firmy LIKE '".$_SESSION["id"]."' ORDER BY typ_souc";
    $vysledek=mysql_query($sql,$link);
    $echo="";
    $i=0;
    while($row = mysql_fetch_array($vysledek)) 
    {
     $echo.="<option value='".$row["id"]."'>".$row["typ_souc"]."</option>";
     $i++;
    }  
    if($i==0) echo "<p class='chyba'>Nejdříve přidejte typ součástky</p>";
     else {                                          
?>
  <div class="form">
   <div>
    <span>* Katalogové číslo:</span>
    <div class="form_d">
     <input type="text" id="form_kat_c" onkeyup="soucOk()">
    </div>
   </div> 
   <div>
    <span>* Typ součástky:</span>
    <div class="form_d">
     <select id="form_typ">
<?php                        
     echo $echo;
?>
     </select>
    </div>
   </div> 
   <div>
    <span>* Hodnota součástky:</span>
    <div class="form_d">
     <input type="text" id="form_hodnota" onkeyup="soucOk()"> 
    </div>
   </div> 
   <div>
    <span>Provedení součástky:</span>
    <div class="form_d">
     <select id="form_provedeni">   
       <option value="">Neurčeno</option>
       <option value="SMD">SMD</option>
       <option value="THT">THT</option>
     </select>
    </div>
   </div> 
   <div>
    <span>Pouzdro součástky:</span>
    <div class="form_d">
     <input type="text" id="form_pouzdro" onkeyup="soucOk()"> 
    </div>
   </div> 
   <div>
    <span>Cena součástky:</span>
    <div class="form_d">
     <input type="text" id="form_cena_kc" onkeyup="soucOk()" size="2"> ,
     <input type="text" id="form_cena_h" onkeyup="soucOk()" size="1" value="00">
     <span>Kč</span>
    </div>
   </div>  
   <div id="souc-ok">
   </div>
  </div>
  <p class="ok">
   Položky označené * jsou povinné
  </p>
<?php
   }
  } 
?>