<?php
 require_once './db.php';
 session_start();
 header('Content-type: text/html; charset=utf-8');
 if(!isset($_SESSION["id"])) echo "<p class='chyba'>Stránka je pouze pro přihlášené firmy</p>";
 else {
   if(isset($_GET["id"])) $tid=$_GET["id"]; 
   else $tid=0;                                          
   echo "<div id='info'>";       
   echo "<span class='x' onclick=\"typX()\"></span>";
   $echo="";
   if(isset($_GET["typ"])){
    $typ=$_GET["typ"];
    $sql="SELECT * FROM typ WHERE id_firmy LIKE '".$_SESSION["id"]."' AND typ_souc LIKE '".$typ."'";
       $vysledek=mysql_query($sql,$link);
       if(($row = mysql_fetch_array($vysledek))) die("<p class='chyba'>Typ součástky již existuje</p>");
        else {
         $sql="UPDATE typ SET typ_souc='".$typ."' WHERE id=".$tid;
         $vysledek=mysql_query($sql,$link) or die ("<p class='chyba'>Chyba při změně názvu typu součástky</p>");    
         $echo.="<p class='ok'>Změny uloženy</p>";
        }
   }       
   if(isset($_GET["souc"])) $s=$_GET["souc"]; 
    else $s=0;
   if(isset($_GET["ntyp"]))
   {
     $sql="UPDATE souc SET typ_souc=".$_GET["ntyp"]." WHERE id=".$s;
     $vysledek=mysql_query($sql,$link) or die("<p class='chyba'>Typ součástky nebyl změněn</p>");
   }
   if(isset($_GET["idso"]))
   {
     $sql="UPDATE souc SET typ_souc=".$tid." WHERE id=".$_GET["idso"];
     $vysledek=mysql_query($sql,$link) or die("<p class='chyba'>Typ součástky nebyl změněn</p>");
   }
   $sql="SELECT id, typ_souc FROM typ WHERE id=".$tid;
   $vysledek=mysql_query($sql,$link) or die ("<p class='chyba'>Chyba při hledání typu součástky</p>");
   $row=mysql_fetch_array($vysledek);
   $echo.="<div>Typ součástky: <input id='typ_uprava' type='text' value='".$row["typ_souc"]."'> <span class='mbtn' onclick=\"typUprava('".$row["id"]."')\">Upravit název</span></div>";
   $echo.="<span class='mbtn' onclick=\"typOdstranit('".$tid."')\">Odstranit</span>";
   echo $echo;
   $echo="<p class='ok'>Po kliknutí můžete změnit typ u dané součástky a poté typ smazat.</p>";
   $echo.="<p>Typ součástky je použit v těchto součástkách:</p>";
   $sql="SELECT t.typ_souc,s.id AS ids,s.kat_c_souc,s.hodnota_souc,s.provedeni_souc,s.pouzdro_souc,s.cena_souc ";
   $sql.="FROM typ t INNER JOIN souc s ON t.id=s.typ_souc WHERE t.id=".$tid." ORDER BY t.typ_souc ASC, s.kat_c_souc ASC";
   $vysledek=mysql_query($sql,$link);
   $i=0;
   $ttr=0;
   $echo.="<table class='table_6'><thead><tr><th>Kat.č.</th><th>Typ</th><th>Hodnota</th><th>Provedení</th><th>Pouzdro</th><th>Cena</th></tr></thead><tbody>";
   while($row=mysql_fetch_array($vysledek))
   {
    if($s==$row["ids"])
    {                               
      $echo.="<tr class='table_tr_".$ttr."'><td>".$row["kat_c_souc"]."</td>";
      $sql="SELECT id, typ_souc FROM typ WHERE id_firmy LIKE '".$_SESSION["id"]."' AND id!=".$tid." ORDER BY typ_souc ASC";
      $vysledek2=mysql_query($sql,$link);
      $echo.="<td><select id='form_typ_info' onchange=\"typSoucUpravaOk('".$tid."','".$row["ids"]."')\"><option value='".$tid."'>".$row["typ_souc"]."</option>";
      while($row2=mysql_fetch_array($vysledek2))
      {
        $echo.="<option value='".$row2["id"]."'>".$row2["typ_souc"]."</option>";
      }
      $echo.="</td>";
    }
    else $echo.="<tr onclick=\"typSoucUprava('".$tid."','".$row["ids"]."')\" class='table_tr_".$ttr."'><td>".$row["kat_c_souc"]."</td><td>".$row["typ_souc"]."</td>";
    $echo.="<td>".$row["hodnota_souc"]."</td><td>".$row["provedeni_souc"]."</td>";
    $echo.="<td>".$row["pouzdro_souc"]."</td><td>".$row["cena_souc"]."</td></tr>";
    $i++;  
    if($ttr==0) $ttr++;
     else $ttr--;
   }
   $echo.="</tbody></table>";
   if($i!=0) echo $echo;
   else echo "<p>Tento typ součástky není nikde použit</p>";

   $echo="<div><h3>Přidání typu součástky do dalších součástek</h3></div>";
   $sql="SELECT DISTINCT so.id,so.kat_c_souc,t.typ_souc,so.hodnota_souc,so.provedeni_souc,so.pouzdro_souc,so.cena_souc ";
   $sql.="FROM souc so INNER JOIN typ t ON t.id=so.typ_souc ";
   $sql.="WHERE t.id NOT IN(SELECT typ_souc FROM souc WHERE typ_souc=".$tid.") AND t.id_firmy LIKE '".$_SESSION["id"]."' ";
   $sql.="ORDER BY t.typ_souc ASC, so.kat_c_souc ASC";
   $vysledek=mysql_query($sql,$link);
   $echo.="<table class='table_7'><thead><tr><th>Kat.č.</th><th>Typ</th><th>Hodnota</th><th>Provedení</th><th>Pouzdro</th><th>Cena</th><th>Přidat</th></tr></thead><tbody>";
   $i=0;
   $ttr=0;
   $row=mysql_fetch_array($vysledek);
   while($row)
   {
    $pom=$row;
    $echo.="<tr><th colspan='8' class='table_td_th'>Typ: ".$row["typ_souc"]."</th></tr>";
    while($pom["typ_souc"]==$row["typ_souc"])
    {  
     $echo.="<tr class='table_tr_".$ttr."'><td>".$row["kat_c_souc"]."</td><td>".$row["typ_souc"]."</td><td>".$row["hodnota_souc"]."</td>";
     $echo.="<td>".$row["provedeni_souc"]."</td><td>".$row["pouzdro_souc"]."</td><td>".$row["cena_souc"]."</td>";
     $echo.="<td><span class='mbtn' onclick=\"typSoucPridat('".$tid."','".$row["id"]."')\">Přidat</span></td>";
    
     if($ttr==0) $ttr++;
       else $ttr--;     
     $row=mysql_fetch_array($vysledek);  
    }
    $i++;
   }
   $echo.="</tbody></table>";
   if($i!=0) echo $echo;
   else echo "<p>Nelze přidat typ součástky do další součástky</p>";        
   echo "</div>";       
 }
?>