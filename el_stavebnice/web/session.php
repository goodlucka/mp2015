<?php
  session_start();
  header('Content-type: text/html; charset=utf-8');
  
  if(empty($_SESSION["cas"])) $_SESSION["cas"] = time();
  if ($_SESSION["cas"] < strtotime("-3 hour"))  
  {
    $_SESSION = array();
    session_destroy();
?>
<script language="javascript" type="text/JavaScript">
  <!--
  setTimeout("location.href='';",0);
  -->
</script>  
<?php
  } else $_SESSION["cas"] = time();
?>