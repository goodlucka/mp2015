<?php        
 require_once './db.php';
 session_start();
 header('Content-type: text/html; charset=utf-8');
 if(!isset($_SESSION["id"])) echo "<p class='chyba'>Stránka je pouze pro přihlášené firmy</p>";
  else {
   echo "<div id='info'>";       
   $id=$_GET["id"];                       
   echo "<span class='x' onclick=\"soucX()\"></span>";
   $echo="";
   if(isset($_GET["idst"])) $idst=$_GET["idst"];
     else $idst=0;
   if(isset($_GET["pocet"]))
   {
     $pocet=(int)rawurldecode($_GET["pocet"]);
     if($pocet<1) echo "<p class='chyba'>Počet kusů musí být větší než 0</p>";
     else {
       if(isset($_GET["idseu"]))
       {
         $sql="UPDATE seznam SET pocet_ks_souc_staveb=".$pocet." WHERE id=".$_GET["idseu"];
         $vysledek=mysql_query($sql,$link);
       }  
       if(isset($_GET["idstp"]))
       {
         $sql="INSERT INTO seznam (id_souc_seznam,id_staveb_seznam,pocet_ks_souc_staveb,id_firmy) VALUES (".$id.",".$_GET["idstp"].",".$pocet.",'".$_SESSION["id"]."')";
         $vysledek=mysql_query($sql,$link);   
       }
     }
   }
   if(isset($_GET["id"])&&isset($_GET["kat_c"]))
   { 
     $kat_c=addslashes(rawurldecode($_GET["kat_c"]));
     $typ=addslashes(rawurldecode($_GET["typ"]));
     $hodnota=addslashes(rawurldecode($_GET["hodnota"])); 
     $provedeni=addslashes(rawurldecode($_GET["provedeni"]));
     $pouzdro=addslashes(rawurldecode($_GET["pouzdro"]));
     $cena_kc=(int)rawurldecode($_GET["cena_kc"]);
     $cena_h=(int)rawurldecode($_GET["cena_h"]);
     $cena=$cena_kc.".".$cena_h;
     $cena=(float)$cena;                     
     $ok=0;
     if(strlen($kat_c)>15 || strlen($kat_c)<1) echo "<p class='chyba'>Katalogové číslo součástky není v daném rozsahu, min. počet znaků je 1, max. počet znaků je 15</p>";
       else $ok++;
     if(strlen($hodnota)>31 || strlen($hodnota)<1) echo "<p class='chyba'>Hodnota součástky není v daném rozsahu, min. počet znaků je 1, max. počet znaků je 31</p>";
       else $ok++;
     if($pouzdro!=NULL)
     {
       if(strlen($pouzdro)>31) echo "<p class='chyba'>Pouzdro součástky je moc dlouhé, max. počet znaků je 31</p>";
        else $ok++;
     } else $ok++;   
     $sql="SELECT id FROM souc WHERE kat_c_souc LIKE '".$kat_c."' AND id_firmy LIKE '".$_SESSION["id"]."' AND id!=".$id;
     $vysledek=mysql_query($sql,$link);
     if(($row=mysql_fetch_array($vysledek))!=null) echo "<p class='chyba'>Zadaná součástka s katalogovým číslem již existuje</p>"; 
       else $ok++;
     if($ok==4)
     {
       $sql="UPDATE souc SET kat_c_souc='".$kat_c."', typ_souc='".$typ."', hodnota_souc='".$hodnota."', provedeni_souc='".$provedeni."',";
       $sql.=" pouzdro_souc='".$pouzdro."', cena_souc='".$cena."' WHERE id=".$id;   
       $vysledek=mysql_query($sql,$link) or die("<p class='chyba'>Chyba při ukládání součástky</p>");
       echo "<p class='ok'>Změny byly uloženy</p>";
     }
   } 
 
   if(isset($_GET["idse"]))
   {
     $sql="DELETE FROM seznam WHERE id=".$_GET["idse"];
     $vysledek=mysql_query($sql,$link) or die("<p>Součástka nebyla odebrána ze stavebnice</p>");
   }
 
   $sql="SELECT t.id AS idt,t.typ_souc, s.id AS ids, s.kat_c_souc, s.hodnota_souc, s.provedeni_souc, s.pouzdro_souc, s.cena_souc ";
   $sql.="FROM typ t INNER JOIN souc s ON t.id=s.typ_souc ";
   $sql.="WHERE s.id_firmy LIKE '".$_SESSION["id"]."' AND s.id=".$id;
   $vysledek=mysql_query($sql,$link) or die("<p class='chyba'>Chyba při hledání součátstky</p>");
   $row=mysql_fetch_array($vysledek);       
   $echo.="<div class='form'><div><span>Katalogové číslo: </span>";
   $echo.=" <div class='form_d'><input type='text' id='form_i_kat_c' value='".$row["kat_c_souc"]."'></div></div>";
   $sql="SELECT id, typ_souc FROM typ WHERE id_firmy LIKE '".$_SESSION["id"]."' AND id!=".$row["idt"];
   $vysledek=mysql_query($sql,$link);
    $echo.="<div><span>Typ: </span> <div class='form_d'><select id='form_i_typ'><option value='".$row["idt"]."'>".$row["typ_souc"]."</option>";
   while($row2=mysql_fetch_array($vysledek))
   {
     $echo.="<option value='".$row2["id"]."'>".$row2["typ_souc"]."</option>";
   }
   $echo.="</select></div></div>";
   $echo.="<div><span>Hodnota: </span><div class='form_d'><input type='text' id='form_i_hodnota' value='".$row["hodnota_souc"]."'></div></div>";
   $echo.="<div><span>Provedení: </span><div class='form_d'><select id='form_i_provedeni'>";
   if($row["provedeni_souc"]=="") $echo.="<option value=''>Neurčeno</option>";
    else $echo.="<option value='".$row["provedeni_souc"]."'>".$row["provedeni_souc"]."</option>";
   if($row["provedeni_souc"]!="") $echo.="<option value=''>Neurčeno</option>";
   if($row["provedeni_souc"]!="SMD") $echo.="<option value='SMD'>SMD</option>";
   if($row["provedeni_souc"]!="THT") $echo.="<option value='THT'>THT</option>";
   $echo.="</select></div></div>";
   $echo.="<div><span>Pouzdro: </span><div class='form_d'><input type='text' id='form_i_pouzdro' value='".$row["pouzdro_souc"]."'></div></div>";
   $pom=explode(".",$row["cena_souc"]);
   if(isset($pom[0])) $cena_kc=$pom[0];
      else $cena_kc=0;
   if(isset($pom[1])) $cena_h=$pom[1];
      else $cena_h=0;
   $echo.="<div><span>Cena: </span><div class='form_d'><input type='text' value='".$cena_kc."' size='2' id='form_i_cena_kc'>,";
   $echo.="<input type='text' value='".$cena_h."' size='1' id='form_i_cena_h'> Kč</div></div>";
   $echo.="<div><span class='mbtn' onclick=\"soucUprava('".$row["ids"]."')\">Upravit součástku</span></div>";
   $echo.="</div>";
   $echo.="<span class='mbtn' onclick=\"soucOdstranit('".$id."')\">Odstranit</span>";
   echo $echo;
                           
   $echo="<p>Součástka je použita v těchto stavebnicích:</p>";
   $sql="SELECT se.id AS idse,st.id,st.kat_c_staveb,st.nazev_staveb,st.cely_nazev_staveb,se.pocet_ks_souc_staveb AS pocet ";
   $sql.="FROM staveb st INNER JOIN seznam se ON st.id=se.id_staveb_seznam WHERE se.id_souc_seznam=".$id." ORDER BY st.nazev_staveb";
   $vysledek=mysql_query($sql,$link);
   $i=0;
   $ttr=0;
   $echo.="<table class='table_5_staveb'><thead><tr><th>Kat.č.</th><th>Název</th><th class='table_cn'>Celý název</th><th>Počet ks</th><th>Odstranit</th></tr></thead><tbody>";
   while($row=mysql_fetch_array($vysledek))
   {
    $echo.="<tr class='table_tr_".$ttr."'><td>".$row["kat_c_staveb"]."</td><td>".$row["nazev_staveb"]."</td>";
    $echo.="<td>".$row["cely_nazev_staveb"]."</td>";       
    if($row["id"]==$idst) $echo.="<td><input type='text' id='i".$row["idse"]."' value='".$row["pocet"]."' onchange=\"soucPocetUpravitOk('".$id."','".$row["idse"]."')\" size=3></td>";
     else $echo.="<td onclick=\"soucPocetUpravit('".$id."','".$row["id"]."')\">".$row["pocet"]."</td>";
    $echo.="<td><span class='mbtn' onclick=\"soucStavebOdstranit('".$id."','".$row["idse"]."')\">Odstranit</span></td>";
    $i++;
    if($ttr==0) $ttr++;
     else $ttr--;
   }
   $echo.="</tbody></table>";
   if($i!=0) echo $echo;
   else echo "<p>Tato součástka není nikde použita</p>"; 
 
   $echo="<div><h3>Přidání součástky do stavebnic</h3></div>";
   $sql="SELECT DISTINCT id,kat_c_staveb,nazev_staveb,cely_nazev_staveb ";
   $sql.="FROM staveb ";
   $sql.="WHERE id NOT IN(SELECT id_staveb_seznam FROM seznam WHERE id_souc_seznam=".$id.") AND id_firmy LIKE '".$_SESSION["id"]."' ";
   $sql.="ORDER BY nazev_staveb ASC";
   $vysledek=mysql_query($sql,$link);
   $echo.="<table class='table_5_staveb'><thead><tr><th>Kat.č.</th><th>Název</th><th class='table_cn'>Celý název</th><th>Počet ks</th><th>Přidat</th></tr></thead><tbody>";
   $i=0;
   $ttr=0;
   while($row=mysql_fetch_array($vysledek))
   {
    $echo.="<tr class='table_tr_".$ttr."'><td>".$row["kat_c_staveb"]."</td><td>".$row["nazev_staveb"]."</td><td>".$row["cely_nazev_staveb"]."</td>";
    $echo.="<td><input type='text' id='poc".$row["id"]."' value='0' size=3></td><td><span class='mbtn' onclick=\"soucStavebPridat('".$id."','".$row["id"]."')\">Přidat</span></td>";
    $i++;
    if($ttr==0) $ttr++;
     else $ttr--;
   }
   $echo.="</tbody></table>";
   if($i!=0) echo $echo;
   else echo "<p>Nelze přidat součástku do další stavebnice</p>";                        
   echo "</div>";       
 }
?>