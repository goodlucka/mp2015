<?php
 require_once './db.php';
 session_start();
 header('Content-type: text/html; charset=utf-8');
 if(!isset($_SESSION["id"])) echo "<p class='chyba'>Stránka je pouze pro přihlášené firmy</p>";
  else {
?> 
<h3>
 Typ součástky
</h3>

  <div id="vyhledavani_hl" class="form">
   <div>
    <span>Typ součástky: </span>
    <input type="text" id="form_typ">
   </div>
   <div>
    <span>Řadit: </span>
    <select id="form_radit">
      <option value="ASC">A->Z</option>
      <option value="DESC">Z->A</option>
    </select>
   </div>
   <div>
    <span class="mbtn" onclick="typVyhledat()">Vyhledat</span>
   </div>
  </div>

  <div id="vyhledavani">
  </div>

  <div id="konec">
  </div>
<?php
  }
?>