<?php
 require_once './db.php';
 header('Content-type: text/html; charset=utf-8');
 session_start();
 
 if(isset($_SESSION["id"])) echo "<p class='chyba'>Firma je již na tomto zařízení přihlášena</p>";
  else
  { 
?>

<h3>
 Přihlášení
</h3>

<div class="form">
 <div>
  <span>Login:</span>
  <input type="text" id="form_id"> 
 </div>
 <div>
  <span>Heslo:</span>
  <input type="password" id="form_heslo">
 </div>
 <div>
  <span class="mbtn" onclick="prihlaseni()">Přihlásit se</span>
 </div>
</div>

<span class="mbtn" href="#" onclick="vypisStranku('./web/zapomenute-heslo.php')">Zapomenuté heslo</span>

<?php
 }
?>