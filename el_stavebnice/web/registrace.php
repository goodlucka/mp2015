<?php
 require_once './db.php';
 header('Content-type: text/html; charset=utf-8');
?>

<h3>
 Registrace
</h3>

<div class="form">
 <div>
  <span>Login:</span>
  <input type="text" id="form_id" onkeyup="regOk()"> 
 </div>
 <div>
  <span title="Tento kód si pečlivě zapamatujte. Pokud zapomenete heslo, použijete tento kód.">Šestimístný kód:</span>
  <input type="password" id="form_kod" size="4" onkeyup="regOk()">
 </div>
 <div id="reg_ok">
 </div>
</div>
<p class="ok">
 Přihlašovací heslo se zobrazí po registraci
</p>
