// JavaScript  
/**********************/
/****** Stránka *******/            
/**********************/                                            
function refresh(stranka)                                       
{                                          
 $("#wb_obsah").load(stranka);                       
 $("#session").load("./web/session.php");
}
function vypisStranku(stranka)
{                      
 $("#wb_obsah").fadeTo(500,0, function() {
   $("#wb_obsah").load(stranka, function() {
     if (stranka=="./web/baleni-souc.php")
       $("#baleni").load("./web/baleni-souc-staveb.php");
     if (stranka=="./web/profil.php")
       zmenit(0);
     $("#wb_obsah").fadeTo(500,1);
     });
  });             
 $("#session").load("./web/session.php"); 
}
function menu(i)
{
 $("ul#wh_menu_ul li").hover(function() {
   jQuery(this).find('.wh_menu_ul_ul').stop(true, true).delay(10).fadeIn(500);
 }, function() {
   jQuery(this).find('.wh_menu_ul_ul').stop(true, true).delay(10).fadeOut(500);
 });
}                  
function baleni(s)   
{
 var stranka;
 if(s=='z') stranka="./web/baleni-souc-staveb.php";
 else stranka="./web/baleni-souc-vypis.php";
 $("#baleni").fadeTo(500,0, function() {
   $("#baleni").load(stranka, function() {
     $("#baleni").fadeTo(500,1);
     });
  });        
}                         
/*******************************/
/****** Správa uživatele *******/     
/*******************************/
function regOk()
{         
 var id=encodeURIComponent(document.getElementById("form_id").value);
 var kod=encodeURIComponent(document.getElementById("form_kod").value);
 $("#reg_ok").load("./web/registrace-ok.php?login="+id+"&kod="+kod);                          
}
function reg()
{                                      
 var id=encodeURIComponent(document.getElementById("form_id").value);
 var kod=encodeURIComponent(document.getElementById("form_kod").value);
 $("#wb_obsah").load("./web/registrace-odeslano.php?id="+id+"&kod="+kod);    
}
function prihlaseni()
{                              
 var id=encodeURIComponent(document.getElementById("form_id").value);
 var heslo=encodeURIComponent(document.getElementById("form_heslo").value);
 $("#wb_obsah").load("./web/prihlaseni-odeslano.php?id="+id+"&heslo="+heslo);
}
function ziskatHeslo()
{                                                                        
 var id=encodeURIComponent(document.getElementById("form_id").value);
 var kod=encodeURIComponent(document.getElementById("form_kod").value);
 $("#wb_obsah").load("./web/zapomenute-heslo.php?id="+id+"&kod="+kod);
}
function kodOk()
{         
 var kod=encodeURIComponent(document.getElementById("form_kod").value);
 $("#profil_ok").load("./web/kod-ok.php?kod="+kod);                          
}
function kodUprav()
{              
 var kod=encodeURIComponent(document.getElementById("form_kod").value);
 $("#wb_obsah").load("./web/profil.php?kod="+kod,
   function(){zmenit(0);});   
}
function hesloOk()
{
 var heslo=encodeURIComponent(document.getElementById("form_heslo").value);
 $("#profil_ok").load("./web/heslo-ok.php?heslo="+heslo);          
}
function hesloUprav()
{
 var heslo=encodeURIComponent(document.getElementById("form_heslo").value);
 $("#wb_obsah").load("./web/profil.php?heslo="+heslo,
   function(){zmenit(0);});      
         
}                                                          
function smazatFirmu()
{
 $("#wb_obsah").load("./web/smazat-firmu-ok.php");
}  
function zmenit(neco)
{                        
  if(neco==0)
  {
    $("#zmenit_kod").slideUp(0);
    $("#zmenit_heslo").slideUp(0);
  }
  if(neco==1)
  {
    $("#zmenit_kod").slideDown(500);
    $("#zmenit_heslo").slideUp(500);
  }
  if(neco==2)
  {
    $("#zmenit_heslo").slideDown(500);
    $("#zmenit_kod").slideUp(500);
  }
}                                 
/****************************/
/****** Typ součástky *******/    
/****************************/
function typOk()                           
{                                                    
 var typ=encodeURIComponent(document.getElementById("form_typ").value);    
 $("#typ_ok").load("./web/typ-ok.php?typ="+typ);                          
}                     
function typPridat()
{
 var typ=encodeURIComponent(document.getElementById("form_typ").value);
 $("#wb_obsah").load("./web/typ-pridat.php?typ="+typ); 
} 
function typVyhledat()
{
 var typ=encodeURIComponent(document.getElementById("form_typ").value);
 var radit=document.getElementById("form_radit").value;
 $("#vyhledavani").fadeTo(200,0, function()
  {$("#vyhledavani").load("./web/typ-vyhledavani-ok.php?typ="+typ+"&radit="+radit,
    function(){
     $("#vyhledavani").fadeTo(500,1);
    });
  });
}
function typInfo(id)
{
 $("#vyhledavani_hl").slideToggle(500);   
 $("#vyhledavani").load("./web/typ-info.php?id="+id);    
}     
function typX()
{    
 var typ=encodeURIComponent(document.getElementById("form_typ").value);
 var radit=document.getElementById("form_radit").value;
 $("#vyhledavani_hl").slideToggle(500, function() 
   {$("#vyhledavani").fadeTo(500,0, function()            
     {$("#vyhledavani").load("./web/typ-vyhledavani-ok.php?typ="+typ+"&radit="+radit).slideDown(0).fadeTo(500,1);
     });
   });
}                       
function typUprava(id)
{
 var typ=encodeURIComponent(document.getElementById("typ_uprava").value);
 $("#vyhledavani").load("./web/typ-info.php?id="+id+"&typ="+typ);
}    
function typOdstranit(id)
{         
 var typ=encodeURIComponent(document.getElementById("form_typ").value);
 var radit=document.getElementById("form_radit").value;   
 $("#vyhledavani_hl").slideToggle(500, function() 
 {$("#vyhledavani").fadeTo(200,0, function() 
  {$("#vyhledavani").load("./web/typ-vyhledavani-ok.php?typ="+typ+"&radit="+radit+"&odstranit="+id,
    function(){
     $("#vyhledavani").fadeTo(500,1);
    });
  });
 });
}
function typSoucUprava(idt,ids)
{
 $("#vyhledavani").load("./web/typ-info.php?id="+idt+"&souc="+ids);
}
function typSoucUpravaOk(idt,ids)
{
 var typ=document.getElementById("form_typ_info").value;
 $("#vyhledavani").load("./web/typ-info.php?id="+idt+"&souc="+ids+"&ntyp="+typ);
}
function typSoucPridat(idt,idso)
{
 $("#vyhledavani").load("./web/typ-info.php?id="+idt+"&idso="+idso);
}
/************************/
/****** Součástka *******/       
/************************/
function soucOk()
{                                  
 var kat_c=encodeURIComponent(document.getElementById("form_kat_c").value);
 var hodnota=encodeURIComponent(document.getElementById("form_hodnota").value);
 var pouzdro=encodeURIComponent(document.getElementById("form_pouzdro").value);
 $("#souc-ok").load("./web/souc-ok.php?kat_c="+kat_c+"&hodnota="+hodnota+"&pouzdro="+pouzdro);
}
function soucPridat()
{
 var kat_c=encodeURIComponent(document.getElementById("form_kat_c").value);
 var hodnota=encodeURIComponent(document.getElementById("form_hodnota").value);
 var pouzdro=encodeURIComponent(document.getElementById("form_pouzdro").value);
 var cena_kc=encodeURIComponent(document.getElementById("form_cena_kc").value);
 var cena_h=encodeURIComponent(document.getElementById("form_cena_h").value);
 var typ=document.getElementById("form_typ").value;
 var provedeni=document.getElementById("form_provedeni").value;
 var odkaz="kat_c="+kat_c+"&hodnota="+hodnota+"&pouzdro="+pouzdro+"&cena_kc="+cena_kc+"&cena_h="+cena_h+"&typ="+typ+"&provedeni="+provedeni;
 $("#wb_obsah").load("./web/souc-pridat.php?"+odkaz);
}                 
function soucUprava(id)
{                                                         
 var kat_c=encodeURIComponent(document.getElementById("form_i_kat_c").value);   
 var typ=encodeURIComponent(document.getElementById("form_i_typ").value);
 var hodnota=encodeURIComponent(document.getElementById("form_i_hodnota").value);
 var provedeni=encodeURIComponent(document.getElementById("form_i_provedeni").value);
 var pouzdro=encodeURIComponent(document.getElementById("form_i_pouzdro").value);
 var cena_kc=encodeURIComponent(document.getElementById("form_i_cena_kc").value);
 var cena_h=encodeURIComponent(document.getElementById("form_i_cena_h").value);
 var odkaz="id="+id+"&kat_c="+kat_c+"&hodnota="+hodnota+"&pouzdro="+pouzdro+"&cena_kc="+cena_kc+"&cena_h="+cena_h+"&typ="+typ+"&provedeni="+provedeni;
 $("#vyhledavani").load("./web/souc-info.php?"+odkaz);
}                   
function soucVyhledat()
{
 var kat_c=encodeURIComponent(document.getElementById("form_kat_c").value);   
 var typ=document.getElementById("form_typ").value;
 var hodnota=encodeURIComponent(document.getElementById("form_hodnota").value);
 var provedeni=encodeURIComponent(document.getElementById("form_provedeni").value);
 var pouzdro=encodeURIComponent(document.getElementById("form_pouzdro").value);
 var cena_od=encodeURIComponent(document.getElementById("form_cena_od").value);
 var cena_do=encodeURIComponent(document.getElementById("form_cena_do").value);
 var radit_dle=document.getElementById("form_radit_dle").value;
 var radit=document.getElementById("form_radit").value;
 var odkaz="kat_c="+kat_c+"&typ="+typ+"&hodnota="+hodnota+"&provedeni="+provedeni+"&pouzdro="+pouzdro+"&cena_od="+cena_od+"&cena_do="+cena_do+"&radit_dle="+radit_dle+"&radit="+radit;
 $("#vyhledavani").fadeTo(200,0, function()
  {$("#vyhledavani").load("./web/souc-vyhledavani-ok.php?"+odkaz,
    function(){
     $("#vyhledavani").fadeTo(500,1);
    });
  });
}
function soucInfo(id)
{                     
 $("#vyhledavani_hl").slideToggle(500); 
 $("#vyhledavani").load("./web/souc-info.php?id="+id);  
}
function soucX()
{                 
 var kat_c=encodeURIComponent(document.getElementById("form_kat_c").value);   
 var typ=document.getElementById("form_typ").value;
 var hodnota=encodeURIComponent(document.getElementById("form_hodnota").value);
 var provedeni=encodeURIComponent(document.getElementById("form_provedeni").value);
 var pouzdro=encodeURIComponent(document.getElementById("form_pouzdro").value);
 var cena_od=encodeURIComponent(document.getElementById("form_cena_od").value);
 var cena_do=encodeURIComponent(document.getElementById("form_cena_do").value);
 var radit_dle=document.getElementById("form_radit_dle").value;
 var radit=document.getElementById("form_radit").value;
 var odkaz="kat_c="+kat_c+"&typ="+typ+"&hodnota="+hodnota+"&provedeni="+provedeni+"&pouzdro="+pouzdro+"&cena_od="+cena_od+"&cena_do="+cena_do+"&radit_dle="+radit_dle+"&radit="+radit;
 $("#vyhledavani_hl").slideToggle(500, function() 
   {$("#vyhledavani").fadeTo(500,0, function()
     {$("#vyhledavani").load("./web/souc-vyhledavani-ok.php?"+odkaz).slideDown(0).fadeTo(500,1);
     });
   });
}
function soucOdstranit(id)
{              
 var kat_c=encodeURIComponent(document.getElementById("form_kat_c").value);   
 var typ=document.getElementById("form_typ").value;
 var hodnota=encodeURIComponent(document.getElementById("form_hodnota").value);
 var provedeni=encodeURIComponent(document.getElementById("form_provedeni").value);
 var pouzdro=encodeURIComponent(document.getElementById("form_pouzdro").value);
 var cena_od=encodeURIComponent(document.getElementById("form_cena_od").value);
 var cena_do=encodeURIComponent(document.getElementById("form_cena_do").value);
 var radit_dle=document.getElementById("form_radit_dle").value;
 var radit=document.getElementById("form_radit").value;
 var odkaz="kat_c="+kat_c+"&typ="+typ+"&hodnota="+hodnota+"&provedeni="+provedeni+"&pouzdro="+pouzdro+"&cena_od="+cena_od+"&cena_do="+cena_do+"&radit_dle="+radit_dle+"&radit="+radit;
 $("#vyhledavani_hl").slideDown(500, function() 
 {$("#vyhledavani").fadeTo(200,0, function() 
  {$("#vyhledavani").load("./web/souc-vyhledavani-ok.php?"+odkaz+"&odstranit="+id,
    function(){
     $("#vyhledavani").fadeTo(500,1);
    });
  });
 });
}           
function soucStavebOdstranit(idso,idse)
{
 $("#vyhledavani").load("./web/souc-info.php?id="+idso+"&idse="+idse);
}     
function soucStavebPridat(idso,idstp)
{
 var pocet=encodeURIComponent(document.getElementById("poc"+idstp).value);  
 $("#vyhledavani").load("./web/souc-info.php?id="+idso+"&idstp="+idstp+"&pocet="+pocet); 
}
function soucPocetUpravit(id,idst)
{
 $("#vyhledavani").load("./web/souc-info.php?id="+id+"&idst="+idst);
}
function soucPocetUpravitOk(id,idse)
{
 var pocet=encodeURIComponent(document.getElementById("i"+idse).value);
 $("#vyhledavani").load("./web/souc-info.php?id="+id+"&idseu="+idse+"&pocet="+pocet);
}
/*************************/
/****** Stavebnice *******/   
/*************************/
function stavebOk()
{
 var kat_c=encodeURIComponent(document.getElementById("form_kat_c").value);
 var nazev=encodeURIComponent(document.getElementById("form_nazev").value);
 var cely_nazev=encodeURIComponent(document.getElementById("form_cely_nazev").value);
 $("#staveb-ok").load("./web/staveb-ok.php?kat_c="+kat_c+"&nazev="+nazev+"&cely_nazev="+cely_nazev);
}
function stavebPridat()
{
 var kat_c=encodeURIComponent(document.getElementById("form_kat_c").value);
 var nazev=encodeURIComponent(document.getElementById("form_nazev").value);
 var cely_nazev=encodeURIComponent(document.getElementById("form_cely_nazev").value);
 $("#wb_obsah").load("./web/staveb-pridat.php?kat_c="+kat_c+"&nazev="+nazev+"&cely_nazev="+cely_nazev);

}              
function stavebUprava(id)
{
 var kat_c=encodeURIComponent(document.getElementById("form_i_kat_c").value);   
 var nazev=encodeURIComponent(document.getElementById("form_i_nazev").value);
 var cely_nazev=encodeURIComponent(document.getElementById("form_i_cely_nazev").value);
 $("#vyhledavani").load("./web/staveb-info.php?kat_c="+kat_c+"&nazev="+nazev+"&cely_nazev="+cely_nazev+"&id="+id);
}            
function stavebVyhledat()
{
 var kat_c=encodeURIComponent(document.getElementById("form_kat_c").value);   
 var nazev=document.getElementById("form_nazev").value;
 var cely_nazev=encodeURIComponent(document.getElementById("form_cely_nazev").value);
 var radit_dle=document.getElementById("form_radit_dle").value;
 var radit=document.getElementById("form_radit").value;
 var odkaz="kat_c="+kat_c+"&nazev="+nazev+"&cely_nazev="+cely_nazev+"&radit="+radit+"&radit_dle="+radit_dle;
 $("#vyhledavani").fadeTo(200,0, function()
  {$("#vyhledavani").load("./web/staveb-vyhledavani-ok.php?"+odkaz,
    function(){
     $("#vyhledavani").fadeTo(500,1);
    });
  });
}
function stavebX()
{  
 var kat_c=encodeURIComponent(document.getElementById("form_kat_c").value);   
 var nazev=document.getElementById("form_nazev").value;
 var cely_nazev=encodeURIComponent(document.getElementById("form_cely_nazev").value);
 var radit_dle=document.getElementById("form_radit_dle").value;
 var radit=document.getElementById("form_radit").value;
 var odkaz="kat_c="+kat_c+"&nazev="+nazev+"&cely_nazev="+cely_nazev+"&radit="+radit+"&radit_dle="+radit_dle;
 $("#vyhledavani_hl").slideToggle(500, function() 
   {$("#vyhledavani").fadeTo(500,0, function()
     {$("#vyhledavani").load("./web/staveb-vyhledavani-ok.php?"+odkaz).slideDown(0).fadeTo(500,1);
     });
   });
}
function stavebInfo(id)
{                      
 $("#vyhledavani_hl").slideToggle(500); 
 $("#vyhledavani").load("./web/staveb-info.php?id="+id);
}
function stavebSoucOdstranit(idst,idse)
{ 
 $("#vyhledavani").load("./web/staveb-info.php?id="+idst+"&idse="+idse);
}     
function stavebOdstranit(id)
{                         
 var kat_c=encodeURIComponent(document.getElementById("form_kat_c").value);   
 var nazev=document.getElementById("form_nazev").value;
 var cely_nazev=encodeURIComponent(document.getElementById("form_cely_nazev").value);
 var radit_dle=document.getElementById("form_radit_dle").value;
 var radit=document.getElementById("form_radit").value;
 var odkaz="kat_c="+kat_c+"&nazev="+nazev+"&cely_nazev="+cely_nazev+"&radit="+radit+"&radit_dle="+radit_dle;
 $("#vyhledavani_hl").slideToggle(500, function() 
 {$("#vyhledavani").fadeTo(200,0, function() 
  {$("#vyhledavani").load("./web/staveb-vyhledavani-ok.php?"+odkaz+"&odstranit="+id,
    function(){
     $("#vyhledavani").fadeTo(500,1);
    });
  });
 });
}
function stavebPocetUpravit(id,idso)
{
 $("#vyhledavani").load("./web/staveb-info.php?id="+id+"&idso="+idso);
}
function stavebPocetUpravitOk(id,idse)
{
 var pocet=encodeURIComponent(document.getElementById("i"+idse).value);
 $("#vyhledavani").load("./web/staveb-info.php?id="+id+"&idseu="+idse+"&pocet="+pocet);
}
function stavebSoucPridat(id,idso)
{
 var pocet=encodeURIComponent(document.getElementById("poc"+idso).value);
 $("#vyhledavani").load("./web/staveb-info.php?id="+id+"&idsop="+idso+"&pocet="+pocet);
}
/*******************************/
/****** Balení stavebnic *******/       
/*******************************/
function pripsaniSoucStaveb(id)
{
 $("#baleni").load("./web/baleni-souc-staveb.php?id="+id)
}
function pripsaniSoucStavebOk(id,i)
{
 var poc_ks=encodeURIComponent(document.getElementById("p"+id).value);
 $("#baleni").load("./web/baleni-souc-staveb.php?idp="+id+"&i="+i+"&poc_ks="+poc_ks)
}
