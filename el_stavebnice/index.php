<!DOCTYPE HTML>
<?php
 session_start();
 header('Content-type: text/html; charset=utf-8');
?>
<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="./styl/css.css" type="text/css">
  <script src="./js/js.js"></script>
  <script src="./js/jquery.js"></script>
  <?php
   require_once './web/db.php';     
  ?>
  
  <title>
    Webová databáze elektronických stavebnic
  </title>
 
 </head>
 <body onload="refresh('./web/index.php')">
  <div id="session">
  </div>
 
   <div id="wh">     
     <div id="wh_h1">
       <h1>
        Elektronické stavebnice
       </h1>
     </div>         
     <div id="wh_log">
     <?php
       if(!isset($_SESSION["id"]))  /* neprihlasena firma */
       {
     ?>
       <a href="#" onclick="vypisStranku('./web/prihlaseni.php')">Přihlášení</a> <span>&times;</span> <a href="#" onclick="vypisStranku('./web/registrace.php')">Registrace</a>
     <?php
       }
       else {          /* prihlasena firma */
         echo "<span><a href=\"#\" onclick=\"vypisStranku('./web/smazat-firmu.php')\">Smazat firmu</a></span><span>Profil firmy <a href='#' onclick=\"vypisStranku('./web/profil.php')\">".$_SESSION["id"]."</a></span> <span><a href='#' onclick=\"vypisStranku('./web/logout.php')\">Odhlásit se</a></span>";
       }
     ?>
     </div>
     <div id="wh_menu">
      <ul id="wh_menu_ul">
       <li class="wh_menu_li" id="i0"><a href="#" class="wh_menu_li_a" onclick="vypisStranku('./web/index.php')" onmouseover="menu(0)" onmouseout="menu(0)">Domů</a></li>
       <li class="wh_menu_li" id="i1"><a href="#" class="wh_menu_li_a" onmouseover="menu(1)" onmouseout="menu(1)">Součástky</a>
         <ul class="wh_menu_ul_ul">                                                                                     
           <li><a href="#" onclick="vypisStranku('./web/typ-pridat.php')">Přidání typu součástky</a></li>  
           <li><a href="#" onclick="vypisStranku('./web/typ-vyhledavani.php')">Vyhledávání typu součástky</a></li>
           <li><a href="#" onclick="vypisStranku('./web/souc-pridat.php')">Přidání součástky</a></li>      
           <li><a href="#" onclick="vypisStranku('./web/souc-vyhledavani.php')">Vyhledávání součástek</a></li>  
         </ul>
       </li>
       <li class="wh_menu_li" id="i2"><a href="#" class="wh_menu_li_a" onmouseover="menu(2)" onmouseout="menu(2)">Stavebnice</a>
         <ul class="wh_menu_ul_ul">
           <li><a href="#" onclick="vypisStranku('./web/staveb-pridat.php')">Přidání stavebnice</a></li>      
           <li><a href="#" onclick="vypisStranku('./web/staveb-vyhledavani.php')">Vyhledávání stavebnic</a></li>
         </ul></li>
       <li class="wh_menu_li" id="i3"><a href="#" class="wh_menu_li_a" onclick="vypisStranku('./web/baleni-souc.php')" onmouseover="menu(3)" onmouseout="menu(3)">Balení součástek</a></li>
      </ul>
     </div>
   </div>
   
   <div id="wb">
    <div id="wb_obsah">
    </div>
   </div>   
   
   <div id="wf">
     <p>
      Lucie Poláchová, I4C
      <br>
      Dlouhodobá maturitní práce s obhajobou
      <br>
      Číslo a název práce: <span id="wf_b">29. Webová databáze elektronických stavebnic</span>
     </p>
   </div>
   
 </body>
</html>